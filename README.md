# DKMS4SSI


## Intro 

A Decentralized Key Management Infrastructure (DKMI) is essential to any SSI
implementation. DKMI is the cryptographic infrastructure upon which the necessary
authentication keys are exchanged between SSI actors. In 2020, a new type of ultra-secure,
ledger agnostic DKMI made its way through the SSI community: Key Event Receipt
Infrastructure (KERI). Currently being developed by DIF (the Decentralized Identity
Foundation) KERI is making its first appearance in real world applications as the first
truly & fully decentralized identity system.

## Summary

🡺 With KERI Ecosystem components developers of SSI solutions will be able to:
- move between networks and ledgers with your identifiers (can work with blockchain and non-blockchained networks)
- delegate identifier to another identity (no matter on which network or system it was created)
- provide key provenance log for production ready Verifiable Credential management
- recovery from key lost through pre-rotation event
- use your identifiers at scale
- resolve any identifier through Distributed Hash Table algorithms without relying on centralize resolver or network.
- operate with any type of Verifiable Credentials across network

![Summary](summary.png "Architecture")

- Resolver - A resolver is the controller of its own self-referential identiﬁer which must always be different from the identiﬁer to be resolved. A resolver provides discoverability for identiﬁers.
- Witness - A Witness is designated (trusted) by the controller of an identiﬁer. The primary role of a witness is to verify, sign, and  maintain events associated with an identiﬁer.
- Juror - A Juror performs duplicity detection on events and event receipts. 
- Verifier - A Veriﬁer cryptographically veriﬁes the signature(s) contained in an event message.
- Validator - A Validator determines if a given signed statement associated with an identiﬁer was valid at the time of issuance.
- Watcher - A Watcher keeps a copy of a KERL for an identiﬁer but is not designated by the controller as one of its witnesses.
- Judge - A Judge examines the entries of one or more KERLs and DELs of a given identiﬁer to validate that the event history comes from a non-duplicitous controller and has been witnessed by a sufﬁcient number of non-duplicitous witnesses such that it may be rusted (or, conversely, not trusted) by a validator.

## Current problems


🡺 **INTEROPERABILITY** Individuals, businesses and entities do not want to be locked into one specific
network or system. This is the expectation raised by the SSI technology. Currently there is not a unified
solution among developing platforms.

🡺 **SECURITY** Multiplicity of architectures makes it difficult to achieve a high level of security for
decentralized identifiers. Over 105 different DID methods are currently available. Each requires a different
type of the resolver hiding the complexity of the resolution process. A solution created in one network may
not be portable. Each leads to solutions claiming a decentralised approach but most are concentrating on
a given DLT and bring additional cross-network communication complexities.


## Solutions

🡺 **INTEROPERABILITY** - KERI introduces a ledger/network agnostic DKMI which includes a root-of-trust in self-certifying
identifiers. Due to it’s security feature it can be used with or without ledger as well as with any type of centralized systems (e.g.
eIDas)

🡺 **SECURITY** - KERI introduces the concept of Autonomic Identity System with Autonomic Identifiers, a primary root-of-trust in
self-certifying identifiers provides strong security base without need to relay on any other network components. KERI minimally
sufficient design principle provides a candidate for trust spanning layer for the internet. The primary root-of-trust are self-certifying
identifiers that are strongly bound at issuance to a cryptographic signing (public, private) key-pair. These are self-contained
until/unless control needs to be transferred to a new key-pair. In that event an append only chained key-event log of signed
transfer statements provides end verifiable control provenance.This makes intervening operational infrastructure replaceable.


For more information see the [slides](eSSIF-lab_-_KERI_components.pdf)
